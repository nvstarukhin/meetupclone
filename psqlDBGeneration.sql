\c postgres
drop database "meetupclone";
create database "meetupclone";
\c meetupclone

create table "user"(
    id serial primary key,
    firstName varchar(255),
    lastName varchar(255),
    email varchar(255) not null unique,
    hashed varchar(255) not null unique
);

create table "group"(
    id serial primary key,
    groupName varchar(255) not null unique,
    groupDesc text not null,
    founderId integer not null,
    CONSTRAINT fk_user
        foreign key(founderId)
            references "user"(id)
            ON DELETE CASCADE
);


create table usergroup(
    userId integer not null references "user"(id) on delete cascade,
    groupId integer not null references "group"(id) on delete cascade,
    primary key(userid, groupid)

);

create table topic(
    id serial primary key,
    name varchar(255) not null unique
);

create table grouptopic(
    groupId integer not null references "group"(id) on delete cascade,
    topicId integer not null references topic(id) on delete cascade,
    primary key(groupId,topicId)
);

create table usertopic(
    userId integer not null references "user"(id) on delete cascade,
    topicId integer not null references topic(id) on delete cascade,
    primary key(userId,topicId)
);

create table comment(
    id serial primary key,
    groupId integer not null references "group"(id) on delete cascade,
    userId integer not null references "user"(id) on delete cascade,
    text text not null,
    timeAt TIMESTAMP default now()
);

create table message(
    id serial primary key,
    senderId integer not null references "user"(id) on delete cascade,
    receiverId integer not null references "user"(id) on delete cascade,
    text text not null,
    timeAt TIMESTAMP default now()
);

create table notification(
    id serial primary key,
    groupId integer not null references "group"(id) on delete cascade,
    userId integer not null references "user"(id) on delete cascade,
    text text not null,
    timeAt TIMESTAMP default now()
);

create table meeting(
    id serial primary key,
    groupId integer not null references "group"(id) on delete cascade,
    description text not null,
    timeAt TIMESTAMP default now()
    );

-- Populating db with users

--This user's password is "password"
insert into "user"(firstName, lastName, email, hashed) values ('Ivan', 'Ivanov', 'example@email.com', '$2a$12$WGWFRHmsJdcaB7CaBUK/eOsz0Ju.N/uX2EAQ3iRx07l9q5DWUyFXm');
--This user's password is "newpassword"
insert into "user"(firstName, lastName, email, hashed) values ('John', 'Doe', 'b@email.com', '$2a$12$Fve7CCEad2tJpr3lFJ9PVeszifYkHcLIxbUX3t6K/bXHxuifDGVVa');


-- Populating db with groups
insert into "group"(groupName,groupDesc,founderId) values('Martial arts','Description is redundant',1);
insert into "group"(groupName,groupDesc,founderId) values('Scala','newbie => pro',2);

-- Populating db with topics
insert into topic(name) values('adventure');
insert into topic(name) values('sport');
insert into topic(name) values('knowledge');
insert into topic(name) values('science');
insert into topic(name) values('books');


-- Populating db with user <-> group relations
insert into usergroup values(1,1);
insert into usergroup values(2,1);

-- Populationg db with group <-> topic relations
insert into grouptopic values (1,1);
insert into grouptopic values (1,2);
insert into grouptopic values (1,3);

insert into grouptopic values (2,3);
insert into grouptopic values (2,4);
insert into grouptopic values (2,5);

-- Populationg db with user <-> topic relations
insert into usertopic values (1,1);
insert into usertopic values (1,2);
insert into usertopic values (1,3);
insert into usertopic values (2,3);




