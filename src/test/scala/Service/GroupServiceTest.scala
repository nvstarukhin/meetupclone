package Service

import meetupclone.Data.DataHandler
import meetupclone.Data.Tables.Notification
import meetupclone.SampleRepo
import meetupclone.Services.Groups.GroupServiceImpl
import meetupclone.Services.Groups.GroupsRepr
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

import scala.concurrent.Future


class GroupServiceTest extends SampleRepo with AsyncMockFactory {

  behavior of "getAllGroups"

  it should "Return a valid list of groups" in {
    (mockGroupHandler.getAllGroups _)
      .expects()
      .returns(Future.successful(SampleGroups.take(1)))

    (mockGroupHandler.getGroupTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    for {
      result <- service.getAllGroups
    } yield result shouldBe Seq(groupRepr)


  }

  behavior of "getGroupById"

  it should "Return a valid group by id" in {
    (mockGroupHandler.getGroupById _)
      .expects(1)
      .returns(Future.successful(SampleGroups(0)))

    (mockGroupHandler.getGroupTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    for {
      result <- service.getGroupById(1)
    } yield result shouldBe groupRepr

  }

  behavior of "findGroupsByName"

  it should "Return a list of groups matching a provided pattern" in {
    (mockGroupHandler.findGroupsByPattern _)
      .expects("%GroupName1%")
      .returns(Future.successful(Seq(SampleGroups(0))))

    (mockGroupHandler.getGroupTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    for {
      result <- service.findGroupsByName("%GroupName1%")
    } yield result shouldBe Seq(groupRepr)
  }

  behavior of "createGroup"

  it should "Successfully create a group from groupRepr" in {

    (mockGroupHandler.createGroup _)
      .expects(SampleGroups(0).copy(id = 0), 1)
      .returns(Future.successful(5))

    (mockGroupHandler.updateGroupTopics _)
      .expects(SampleTopics.take(2).map(_.name), 5)
      .returns(Future.successful(SampleTopics.take(2)))

    (mockGroupHandler.getGroupById _)
      .expects(5)
      .returns(Future.successful(SampleGroups(0).copy(id = 5)))

    (mockGroupHandler.getUsersByTopics _)
      .expects(SampleTopics.take(2).map(_.name))
      .returns(Future.successful(SampleUsers.drop(1).map(_.id)))

    (mockGroupHandler.sendNotification _)
      .expects(*)
      .returns(Future.successful(true))
      .twice


    for {
      result <- service.createGroup(groupRepr, 1)
    } yield result shouldBe 5

  }

  behavior of "findGroupsByTopics"

  it should "Successfully return groups matching given topics" in {
    (mockGroupHandler.getGroupsByTopics _)
      .expects(SampleTopics.take(2).map(_.name))
      .returns(Future.successful(SampleGroups.take(1)))

    (mockGroupHandler.getGroupTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    for {
      result <- service.findGroupsByTopics(SampleTopics.take(2).map(_.name))
    } yield result shouldBe Seq(groupRepr)
  }


  // ------------------ INTERNAL ---------------------
  private val mockGroupHandler: DataHandler = mock[DataHandler]
  private val service = new GroupServiceImpl(mockGroupHandler)
  private val groupRepr = GroupsRepr(Some(1), SampleGroups(0).groupname, SampleGroups(0).groupdesc, Some(1), Some(SampleTopics.take(2).map(_.name)))


}
