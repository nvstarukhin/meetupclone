package DB

import meetupclone.{GroupNotFoundException, NameAlreadyExistsException, NotAValidMeetingExeption, UserNotFoundException}

import scala.concurrent.Future
import meetupclone.Data.Tables.{Group, Meeting}

class DBGroupsTest extends DBSuite {

  behavior of "getAllGroups"

  it should "Return all groups" in {
    for {
      db <- test()
      groups <- h(db).getAllGroups()
        .andThen{ case _ =>db.close() }
    } yield groups shouldBe SampleGroups
  }


  behavior of "getGroupById"

  it should "Return a group on valid ID" in {
    for {
      db <- test()
      groups <- h(db).getGroupById(1)
        .andThen{ case _ =>db.close() }
    } yield groups shouldBe SampleGroups(0)
  }

  it should "fail with GroupNotFoundException if no such group exists" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        groups <- h(db).getGroupById(10)
          .andThen{ case _ =>db.close() }
      } yield groups
    }
  }

  behavior of "findGroupsByName"

  it should "return list of groups" in {
    for {
      db <- test()
      groups <- h(db).findGroupsByPattern("%Name%")
        .andThen{ case _ =>db.close() }
    } yield groups shouldBe SampleGroups
  }

  it should "return list containing a single group matching provided pattern" in {
    for {
      db <- test()
      groups <- h(db).findGroupsByPattern("%2%")
        .andThen{ case _ =>db.close() }
    } yield groups shouldBe Seq(SampleGroups(1))
  }

  behavior of "createGroup"

  it should "successfully create a group" in {
    val newGroup = Group(0, "NewGroup", "New Group Desc", 3)
    for {
      db <- test()
      _ <- h(db).createGroup(newGroup, 3)
      result <- h(db).getAllGroups()
        .andThen{ case _ =>db.close() }
    } yield result.map(_.groupname) shouldBe SampleGroups.appended(newGroup).map(_.groupname)
  }

  it should "successfully create a userGroup connection" in {
    val newGroup = Group(0, "NewGroup", "New Group Desc", 3)
    for {
      db <- test()
      _ <- h(db).createGroup(newGroup, 3)
      _ <- h(db).getAllGroups()
      result <- h(db).getUserGroupsById(3)
        .andThen{ case _ =>db.close() }
    } yield result.map(_.groupname) shouldBe Seq(newGroup.groupname)
  }

  it should "fail with a UserNotFoundException if no such user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      val newGroup = Group(0, "NewGroup", "New Group Desc", 3)
      for {
        db <- test()
        _ <- h(db).createGroup(newGroup, 10)
        _ <- h(db).getAllGroups()
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  it should "fail with a  NameAlreadyExistsException if group with this name already exists" in {
    recoverToSucceededIf[NameAlreadyExistsException] {
      val newGroup = Group(0, "GroupName1", "New Group Desc", 3)
      for {
        db <- test()
        _ <- h(db).createGroup(newGroup, 3)
        _ <- h(db).getAllGroups()
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  behavior of "getAllComments"

  it should "return all of groups comments" in {
    for {
      db <- test()
      result <- h(db).getAllComments(1)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe SampleComments.take(3)
  }

  it should "fail with a GroupNotFoundException if no group with matching id" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        result <- h(db).getAllComments(10)
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  behavior of "addComment"

  it should "successfully add a comment" in {
    for {
      db <- test()
      _ <- h(db).addComment(SampleComments(0).copy(text = "new comment added"))
      result <- h(db).getAllComments(SampleComments(0).groupid)
        .andThen{ case _ =>db.close() }
    } yield result should contain(SampleComments(0).copy(text = "new comment added", id = 6))
  }


  behavior of "sendNotification"

  it should "successfully send a notification" in {
    for {
      db <- test()
      _ <- h(db).sendNotification(SampleNotifications(0).copy(text = "New Notification"))
      result <- h(db).getUserNotifications(1)
        .andThen{ case _ =>db.close() }
    } yield result.map(_.text) should contain("New Notification")
  }

  it should "fail if user does not exist" in {

    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        _ <- h(db).sendNotification(SampleNotifications(0).copy(text = "New Notification", userid = 10))
        _ <- h(db).getUserNotifications(10)
          .andThen{ case _ =>db.close() }
      } yield ()

    }

  }

  it should "fail if group does not exist" in {

    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        _ <- h(db).sendNotification(SampleNotifications(0).copy(text = "New Notification", userid = 1, groupid = 10))
          .andThen{ case _ =>db.close() }
      } yield ()

    }

  }

  behavior of "create Meeting"

  it should "successfully create a meeting" in {
  val sampleMeeting = Meeting(1, 2, "Description", None)
    for {
      db <- test()
      result <- h(db).createMeeting(sampleMeeting).andThen {case _ => db.close()}
    } yield result shouldBe true
  }

  it should "fail if no such group exists" in {
    recoverToSucceededIf[GroupNotFoundException] {
      val sampleMeeting = Meeting(1, 10, "Description", None)
      for {
        db <- test()
        result <- h(db).createMeeting(sampleMeeting).andThen {case _ => db.close()}
      } yield ()
    }
  }

  it should "fail if meeting already exists" in {
    recoverToSucceededIf[NotAValidMeetingExeption] {
      val sampleMeeting = Meeting(1, 2, "Description", None)
      for {
        db <- test()
        _ <- h(db).createMeeting(sampleMeeting)
        result <- h(db).createMeeting(sampleMeeting).andThen {case _ => db.close()}
      } yield ()
    }
  }





}
