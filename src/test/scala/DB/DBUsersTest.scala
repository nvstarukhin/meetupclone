package DB

import akka.actor.Status.Success
import meetupclone.{EmailAlreadyExistsException, GroupNotFoundException, UserNotFoundException, WrongCredentialsException}

import scala.concurrent.Future
import scala.util.Failure


class DBUsersTest extends DBSuite {

  behavior of "getUserById"

  it should "return a user if such user exists in a DB" in {
     for {
      db <- test()
      user <- h(db).getUserById(1)
        .andThen{ case _ =>db.close() }
    } yield assert(user == SampleUsers(0))


  }
  it should "fail with UserNotFoundException if no user with such IP Exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        user <- h(db).getUserById(10)
          .andThen{ case _ =>db.close() }
      } yield user
    }
  }

  behavior of "getAllUsers"

  it should "get a Sequence of all Users" in {
    for {
      db <- test()
      users <- h(db).getAllUsers()
        .andThen{ case _ =>db.close() }
    } yield assert(users == SampleUsers)

  }

  behavior of "createUser"

  it should "successfully create a new User" in {
    for {
      db <- test()
      result <- h(db).createUser("uniqueEmail", "uniqueHash")
        .andThen{ case _ =>db.close() }
    } yield assert(result == 4)
  }

  it should "return a EmailAlreadyExistsExeption if email already exists" in {
    recoverToSucceededIf[EmailAlreadyExistsException] {
      for {
        db <- test()
        result <- h(db).createUser("email1@gmail.com", "uniqueHash")
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  behavior of "getUserByEmail"

  it should "successfully get a User with matching email" in {
    for {
      db <- test()
      user <- h(db).getUserByEmail("email1@gmail.com")
        .andThen{ case _ =>db.close() }
    } yield assert(user == SampleUsers(0))
  }

  it should "fail with a WrongCredentialsException if no user with such email" in {
    recoverToSucceededIf[WrongCredentialsException] {
      for {
        db <- test()
        result <- h(db).getUserByEmail("nonexistant email")
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  behavior of "updateUser"

  it should "successfully update a valid user" in {
    for {
      db <- test()
      _ <- h(db).updateUser(SampleUsers(0).copy(firstname = Some("UpdatedName")))
      updated <- h(db).getUserById(SampleUsers(0).id)
        .andThen{ case _ =>db.close() }
    } yield updated shouldBe SampleUsers(0).copy(firstname = Some("UpdatedName"))
  }

  it should "fail with a UserNotFoundException if no such user exists in storage" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        result <- h(db).updateUser(SampleUsers(0).copy(id = 10))
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  it should "fail with a EmailAlreadyExistsException if provided email is occupied by another user" in {
    recoverToSucceededIf[EmailAlreadyExistsException] {
      for {
        db <- test()
        result <- h(db).updateUser(SampleUsers(0).copy(email = "email2@gmail.com"))
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  behavior of "getUserGroupsById"

  it should "get user's groups on valid user id" in {
    for {
      db <- test()
      result <- h(db).getUserGroupsById(1)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe SampleGroups.take(3)

  }

  it should "return empty sequence if user has no groups" in {
    for {
      db <- test()
      result <- h(db).getUserGroupsById(3)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe Seq()
  }

  it should "fail with UserNotFoundException if no such user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        result <- h(db).getUserGroupsById(10)
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  behavior of "joinGroup"


  it should "successfully join a group" in {
    for {
      db <- test()
      _ <- h(db).joinGroup(1, 3)
      result <- h(db).getUserGroupsById(3)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe SampleGroups.take(1)
  }

  it should "fail with UserNotFoundException if no user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        _ <- h(db).joinGroup(1, 10)
        result <- h(db).getUserGroupsById(3)
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  it should "fail with GroupNotFoundException if no group exists" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        _ <- h(db).joinGroup(10, 1)
        result <- h(db).getUserGroupsById(3)
          .andThen{ case _ =>db.close() }
      } yield result
    }
  }

  behavior of "leaveGroup"

  it should "successfully leave a group" in {
    for {
      db <- test()
      _ <- h(db).leaveGroup(1, 1)
      result <- h(db).getUserGroupsById(1)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe SampleGroups.take(3).drop(1)
  }

  it should "fail with UserNotFoundException if no user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        _ <- h(db).leaveGroup(1, 10)
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  it should "fail with GroupNotFoundException if no group exists" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        _ <- h(db).leaveGroup(10, 1)
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  behavior of "addComment"

  it should "successfully add a comment" in {
    for {
      db <- test()
      _ <- h(db).addComment(SampleComments(0).copy(text = "BrandNewText"))
      result <- h(db).getAllComments(1)
        .andThen{ case _ =>db.close() }
    } yield result.map(_.text) should contain("BrandNewText")
  }

  it should "fail with GroupNotFoundException" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        _ <- h(db).addComment(SampleComments(0).copy(groupid = 10))
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  it should "fail with UserNotFoundException" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        _ <- h(db).addComment(SampleComments(0).copy(userid = 10))
          .andThen{ case _ =>db.close() }
      } yield ()
    }
  }

  behavior of "getAllUserMessages"

  it should "successfully get all user related messages" in {
    for {
      db <- test()
      result <- h(db).getAllUserMessages(1)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe (SampleMessages.take(2))
  }

  it should "successfully get only received messages if user did not send any" in {
    for {
      db <- test()
      result <- h(db).getAllUserMessages(3)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe (SampleMessages.drop(1))
  }

  it should "fail with UserNotFoundException if no such user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        result <- h(db).getAllUserMessages(10)
          .andThen{ case _ =>db.close() }
      } yield ()

    }

  }

  behavior of "getUserNotifications"

  it should "successfully get all user related notifications" in {
    for {
      db <- test()
      result <- h(db).getUserNotifications(1)
        .andThen{ case _ =>db.close() }
    } yield result shouldBe (SampleNotifications.take(2))
  }

  it should "fail with UserNotFoundException if no such user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        result <- h(db).getUserNotifications(10)
          .andThen{ case _ =>db.close() }
      } yield ()

    }

  }





}
