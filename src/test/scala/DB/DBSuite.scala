package DB

import java.util.UUID

import meetupclone.Data.SlickDataHandler
import meetupclone.Data.Tables._
import org.scalactic.source
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.{Assertion, compatible}
import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import meetupclone.SampleRepo

import scala.concurrent.Future

class DBSuite extends SampleRepo with Matchers {

  def h(db: Database) = new SlickDataHandler(db)

  def test() = {

    val db = Database.forURL(
      s"jdbc:h2:mem:${UUID.randomUUID()}",
      driver = "org.h2.Driver",
      keepAliveConnection = true
    )

    db.run(initSchema
      .andThen(UserTable ++= SampleUsers)
      .andThen(GroupTable ++= SampleGroups)
      .andThen(UsergroupTable ++= SampleUserGroups)
      .andThen(TopicTable ++= SampleTopics)
      .andThen(UsertopicTable ++= SampleUserTags)
      .andThen(GrouptopicTable ++= SampleGroupTags)
      .andThen(CommentTable ++= SampleComments)
      .andThen(MessageTable ++= SampleMessages)
      .andThen(NotificationTable ++= SampleNotifications))
      .flatMap[Database](_ => Future.successful(db))
  }


  private val initSchema = {
    (UserTable.schema ++ GroupTable.schema ++ UsergroupTable.schema ++
      TopicTable.schema ++ UsertopicTable.schema ++ GrouptopicTable.schema ++
      CommentTable.schema ++ MessageTable.schema ++ NotificationTable.schema ++
      MeetingTable.schema).create
  }


}
