package meetupclone

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

sealed case class Credentials(email: String, password: String)

case object Credentials {
  implicit val jsonDecoder: Decoder[Credentials] = deriveDecoder
  implicit val jsonEncoder: Encoder[Credentials] = deriveEncoder
}