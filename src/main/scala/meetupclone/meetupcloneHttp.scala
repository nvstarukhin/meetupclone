package meetupclone

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import meetupclone.Data.SlickDataHandler
import meetupclone.Routes.{AuthRoutes, GroupRoutes, MeetupcloneExceptionHandler, UserRoutes}
import meetupclone.Services.Auth.AuthServiceImpl
import meetupclone.Services.Groups.GroupServiceImpl
import meetupclone.Services.Users.UserServiceImpl

import scala.concurrent.ExecutionContextExecutor

object meetupcloneHttp extends App {

  import slick.jdbc.JdbcBackend.Database

  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContextExecutor = ac.dispatcher

  //Using PostgreSQL
  val db = Database.forConfig("meetupclone")

  // Handles data
  val dataHandler = new SlickDataHandler(db)
  // Handles exceptions
  val exceptionHandler = MeetupcloneExceptionHandler

  // Services
  val authService = new AuthServiceImpl(dataHandler)
  val userService = new UserServiceImpl(dataHandler)
  val groupService = new GroupServiceImpl(dataHandler)


  //Routes
  val authRoutes = new AuthRoutes(authService).routes
  val userRoutes = new UserRoutes(userService).routes
  val groupRoutes = new GroupRoutes(groupService).routes


  val route = authRoutes ~ userRoutes ~ groupRoutes

  val routes: Route = Route.seal(route)(exceptionHandler = exceptionHandler.eh)

  Http().newServerAt("localhost", 8080)
    .bind(routes)


}
