package meetupclone.Routes

import akka.http.scaladsl.server.Route
import meetupclone.Services.Groups.GroupService
import meetupclone.Services.Groups.GroupsRepr

import scala.concurrent.ExecutionContext

class GroupRoutes(service: GroupService)(implicit ec: ExecutionContext) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  private val sh = new SessionHandler()


  val routes: Route = pathPrefix("groups") {
    concat(
      getAllGroupsRoute,
      findGroupsByName,
      createGroup,
      findGroupsByTopics,
      ID
    )
  }

  private val ID = pathPrefix(LongNumber) { groupId =>
    concat(
      get {
        concat(getGroupById(groupId),
          getGroupTopicsById(groupId),
          getGroupCommentsById(groupId))
      },
      post {
        concat(
          addComment(groupId),
          addMeeting(groupId)
        )
      }
    )
  }


  private def getGroupById(groupId: Long): Route = pathEnd {

    complete(service.getGroupById(groupId))

  }

  private def getGroupTopicsById(groupId: Long): Route = path("topics") {

    complete(service.getGroupTopicsById(groupId))

  }


  private def getGroupCommentsById(groupId: Long): Route = path("comments") {
    complete(service.getAllComments(groupId))

  }

  private def addComment(groupId: Long): Route = path("comments" / "add") {
    sh.withSession { session =>
      entity(as[String]) { text =>
        complete(service.addComment(text, groupId, session))
      }

    }
  }

  private def addMeeting(groupId: Long): Route = path("meetings") {
    sh.withSession { session =>
      entity(as[(String, String)]) { input =>
        complete(service.createMeeting(input._1, session, groupId, input._2))
      }
    }
  }

  private val getAllGroupsRoute: Route = path("all") {
    get {
      complete(service.getAllGroups)
    }
  }

  private val findGroupsByName: Route =
    parameter("name") { name =>
      get {
        complete(service.findGroupsByName(name))
      }
    }


  private val createGroup: Route = path("create") {
    post {
      sh.withSession { session =>
        entity(as[GroupsRepr]) { group =>
          complete(service.createGroup(group, session))
        }
      }
    }
  }

  private val findGroupsByTopics: Route = path("discoverByTopics") {
    get {
      parameters("topic".repeated) { topics =>
        complete(service.findGroupsByTopics(topics.toSeq))
      }
    }
  }


}
