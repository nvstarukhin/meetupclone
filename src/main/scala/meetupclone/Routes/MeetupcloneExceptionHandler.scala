package meetupclone.Routes

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler
import meetupclone._

object MeetupcloneExceptionHandler {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val eh: ExceptionHandler =
    ExceptionHandler {
      case e: NotFoundException => complete(NotFound, ExceptionResponse(e.getMessage))
      case e: AlreadyExistsException => complete(Conflict, ExceptionResponse(e.getMessage))
      case e: RestrictedException => complete(Forbidden, ExceptionResponse(e.getMessage))
      case e: NotAValidException => complete(BadRequest, ExceptionResponse(e.getMessage))
      case e: MeetupCloneException => complete(BadRequest, ExceptionResponse(e.getMessage))
    }
}
