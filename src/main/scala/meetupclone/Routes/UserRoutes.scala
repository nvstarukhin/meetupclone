package meetupclone.Routes

import akka.http.scaladsl.server.Route
import meetupclone.Services.Users.{UserService, UsersRepr}

import scala.concurrent.ExecutionContext

class UserRoutes(service: UserService)(implicit ec: ExecutionContext) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._


  val routes: Route = pathPrefix("users") {
    concat(ID, getAllUsers)
  }


  private val sh = new SessionHandler()


  private val ID = pathPrefix(LongNumber) { userId =>
    concat(
      get {
        concat(
          getUserById(userId),
          getUserTopicsById(userId),
          getUserGroupsById(userId),
          getAllUserMessages(userId),
          getUserNotifications(userId)
        )
      },

      post {
        concat(
          updateUser(userId),
          updateUserTopics(userId),
          joinGroup(userId),
          leaveGroup(userId)
        )
      }

    )
  }

  private def getUserById(userId: Long): Route = pathEnd {
    complete(service.getUserById(userId))
  }

  private def getAllUserMessages(userId: Long): Route = path("messages") {
    sh.withSession { session =>
      def verified = sh.verify(session, userId)

      complete(verified.flatMap(_ => service.getAllUserMessages(userId)))
    }
  }

  private def getUserNotifications(userId: Long): Route = path("notifications") {
    sh.withSession { session =>
      def verified = sh.verify(session, userId)

      complete(verified.flatMap(_ => service.getUserNotifications(userId)))
    }
  }


  private def getUserTopicsById(userId: Long): Route = path("topics") {
    complete(service.getUserTopicsById(userId))
  }

  private def getUserGroupsById(userId: Long): Route = path("groups") {
    complete(service.getUserGroupsById(userId))
  }

  private def updateUser(userId: Long): Route = path("update") {
    sh.withSession { session =>
      entity(as[UsersRepr]) { userFromAPi =>
        def v = sh.verify(session, userId)

        complete(v.flatMap(_ => service.updateUser(userFromAPi, userId)))
      }
    }

  }

  private def updateUserTopics(userId: Long): Route = path("updateTopics") {
    sh.withSession { session =>
      entity(as[Seq[String]]) { t =>
        def v = sh.verify(session, userId)

        complete(v.flatMap(_ => service.updateUserTopics(t, userId)))
      }
    }

  }

  private def joinGroup(userId: Long): Route = path("joinGroup") {
    parameter("groupId".as[Long]) { groupId =>
      sh.withSession { session =>
        val verified = sh.verify(session, userId)
        complete(verified.flatMap(_ => service.joinGroup(userId, groupId)))
      }
    }
  }

  private def leaveGroup(userId: Long): Route = path("leaveGroup") {
    parameter("groupId".as[Long]) { groupId =>
      sh.withSession { session =>
        val v = sh.verify(session, userId)
        complete(v.flatMap(_ => service.leaveGroup(userId, groupId)))
      }
    }
  }

  private val getAllUsers: Route = path("all") {
    get {
      complete(service.getAllUsers())
    }
  }


}
