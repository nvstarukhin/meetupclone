package meetupclone.Services.Groups



import meetupclone.Data.Tables._
import meetupclone.Services.Groups.GroupsRepr

import scala.concurrent.Future

trait GroupService {


  def getAllGroups: Future[Seq[GroupsRepr]]

  def getGroupById(id: Long): Future[GroupsRepr]

  def findGroupsByName(query: String): Future[Seq[GroupsRepr]]

  def getGroupTopicsById(id: Long): Future[Seq[Topic]]

  def createGroup(group: GroupsRepr, founderId: Long): Future[Long]

  def findGroupsByTopics(tags: Seq[String]): Future[Seq[GroupsRepr]]

  def getAllComments(groupId: Long): Future[Seq[Comment]]

  def addComment(text: String, groupId: Long, userId: Long): Future[Boolean]

  def createMeeting(text: String, userId: Long, groupId: Long, time: String): Future[Boolean]
}
