package meetupclone.Services.Users

import meetupclone.Data.DataHandler
import meetupclone.Data.Tables._

import scala.concurrent.{ExecutionContext, Future}

class UserServiceImpl(handler: DataHandler)(implicit ec: ExecutionContext) extends UserService {

  override def getAllUsers(): Future[Seq[UsersRepr]] = {

    for {
      users<- handler.getAllUsers()
      result <- Future.foldLeft(users.map( user => mapFromUser(user)))(Seq[UsersRepr]()) {
        (a,b) => b+:a
      }
    } yield result
  }

  override def updateUser(user: UsersRepr, id: Long): Future[UsersRepr] = {
    mapToUser(user, id)
      .flatMap(handler.updateUser(_))
      .flatMap(mapFromUser(_))
  }

  override def getUserById(id: Long): Future[UsersRepr] = {
    handler.getUserById(id).flatMap(mapFromUser(_))
  }

  override def getUserTopicsById(id: Long): Future[Seq[Topic]] = {
    handler.getUserTopicsById(id)
  }

  override def updateUserTopics(tags: Seq[String], userId: Long): Future[Seq[Topic]] = {
    handler.updateUserTopics(tags, userId)
  }

  override def getUserGroupsById(id: Long): Future[Seq[Group]] = {
    handler.getUserGroupsById(id)
  }

  override def joinGroup(userId: Long, groupId: Long): Future[Boolean] = {
    handler.joinGroup(groupId, userId)
  }

  override def leaveGroup(userId: Long, groupId: Long): Future[Boolean] = {
    handler.leaveGroup(groupId, userId)
  }

  override def getAllUserMessages(userId: Long): Future[Seq[Message]] = {
    handler.getAllUserMessages(userId)
  }

  override def getUserNotifications(userId: Long): Future[Seq[Notification]] = {
    handler.getUserNotifications(userId)
  }


  // ------------------------------ INTERNAL----------------------------------------------

  private def mapToUser(userFromApi: UsersRepr, userId: Long): Future[User] = {
    handler
      .getUserById(userId)
      .flatMap(user => makeUserFromApi(userFromApi, user))
  }

  private def mapFromUser(user: User): Future[UsersRepr] = {

    val info = for {
      topics <- getUserTopicsById(user.id)
      groups <- getUserGroupsById(user.id)
    } yield (topics, groups)

    info.map { case (topics, groups) => UsersRepr(Some(user.id), Some(user.email), user.firstname, user.lastname, Some(topics), Some(groups)) }

  }

  private def makeUserFromApi(fromApi: UsersRepr, user: User): Future[User] = {

    val email: String = fromApi.email match {
      case Some(value) => value
      case None => user.email
    }

    val firstname = fromApi.firstname match {
      case Some(value) => Some(value)
      case None => user.firstname
    }

    val lastname = fromApi.lastname match {
      case Some(value) => Some(value)
      case None => user.lastname
    }

    Future.successful(user.copy(email = email, firstname = firstname, lastname = lastname))
  }


}
