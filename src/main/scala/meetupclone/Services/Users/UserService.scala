package meetupclone.Services.Users

import meetupclone.Data.Tables._
import scala.concurrent.Future

trait UserService {


  def getAllUsers(): Future[Seq[UsersRepr]]

  def updateUser(user: UsersRepr, userId: Long): Future[UsersRepr]

  def getUserById(id: Long): Future[UsersRepr]

  def getUserTopicsById(id: Long): Future[Seq[Topic]]

  def updateUserTopics(tags: Seq[String], userId: Long): Future[Seq[Topic]]

  def getUserGroupsById(id: Long): Future[Seq[Group]]

  def joinGroup(userId: Long, GroupId: Long): Future[Boolean]

  def leaveGroup(userId: Long, groupId: Long): Future[Boolean]

  def getAllUserMessages(userId: Long): Future[Seq[Message]]

  def getUserNotifications(userId: Long): Future[Seq[Notification]]

}
