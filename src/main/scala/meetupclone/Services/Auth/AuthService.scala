package meetupclone.Services.Auth

import meetupclone.Credentials

import scala.concurrent.Future

trait AuthService {

  def register(credentials: Credentials): Future[Boolean]

  def login(credentials: Credentials): Future[Long]




}
