package meetupclone.Data

import meetupclone.Data.Tables._

import scala.concurrent.Future

trait DataHandler {

  def getUserById(id: Long): Future[User]

  def getUserByEmail(email: String): Future[User]

  def getAllUsers(): Future[Seq[User]]

  def createUser(email: String, hash: String): Future[Long]

  def updateUser(user: User): Future[User]

  def getUserTopicsById(id: Long): Future[Seq[Topic]]

  def updateUserTopics(tags: Seq[String], id: Long): Future[Seq[Topic]]

  def getAllGroups(): Future[Seq[Group]]

  def getGroupById(id: Long): Future[Group]

  def findGroupsByPattern(query: String): Future[Seq[Group]]

  def getUserGroupsById(id: Long): Future[Seq[Group]]

  def getGroupTopicsById(id: Long): Future[Seq[Topic]]

  def createGroup(group: Group, founderId: Long): Future[Long]

  def joinGroup(groupId: Long, userId: Long): Future[Boolean]

  def leaveGroup(groupId: Long, userId: Long): Future[Boolean]

  def getGroupsByTopics(topics: Seq[String]): Future[Seq[Group]]

  def getAllComments(groupId: Long): Future[Seq[Comment]]

  def addComment(comment: Comment): Future[Boolean]

  def getUsersByTopics(tags: Seq[String]): Future[Seq[Long]]

  def updateGroupTopics(tags: Seq[String], groupId: Long): Future[Seq[Topic]]

  def getAllUserMessages(userId: Long): Future[Seq[Message]]

  def sendMessage(message: Message): Future[Boolean]

  def getUserNotifications(userId: Long): Future[Seq[Notification]]

  def sendNotification(notification: Notification): Future[Boolean]

  def createMeeting(meeting: Meeting): Future[Boolean]

  def getGroupsUsers(groupId: Long): Future[Seq[Long]]


}