package meetupclone.Data

// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.PostgresProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile

  import profile.api._
  import io.circe._, io.circe.generic.semiauto._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** Entity class storing rows of table CommentTable
   *
   * @param id      Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param groupid Database column groupid SqlType(int4)
   * @param userid  Database column userid SqlType(int4)
   * @param text    Database column text SqlType(text)
   * @param timeat  Database column timeat SqlType(timestamp) */
  case class Comment(id: Long, groupid: Long, userid: Long, text: String, timeat: Option[java.time.Instant])

  object Comment {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Comment] = deriveDecoder
    implicit val jsonEncoder: Encoder[Comment] = deriveEncoder
  }

  /** GetResult implicit for fetching Comment objects using plain SQL queries */
  implicit def GetResultComment(implicit e0: GR[Long], e1: GR[String], e2: GR[Option[java.time.Instant]]): GR[Comment] = GR {
    prs =>
      import prs._
      Comment.tupled((<<[Long], <<[Long], <<[Long], <<[String], <<?[java.time.Instant]))
  }

  /** Table description of table comment. Objects of this class serve as prototypes for rows in queries. */
  class CommentTable(_tableTag: Tag) extends profile.api.Table[Comment](_tableTag, "comment") {
    def * = (id, groupid, userid, text, timeat) <> (Comment.tupled, Comment.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(groupid), Rep.Some(userid), Rep.Some(text), timeat)).shaped.<>({ r => import r._; _1.map(_ => Comment.tupled((_1.get, _2.get, _3.get, _4.get, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column groupid SqlType(int4) */
    val groupid: Rep[Long] = column[Long]("groupid")
    /** Database column userid SqlType(int4) */
    val userid: Rep[Long] = column[Long]("userid")
    /** Database column text SqlType(text) */
    val text: Rep[String] = column[String]("text")
    /** Database column timeat SqlType(timestamp) */
    val timeat: Rep[Option[java.time.Instant]] = column[Option[java.time.Instant]]("timeat")

    /** Foreign key referencing GroupTable (database name comment_groupid_fkey) */
    lazy val groupTableFk = foreignKey("comment_groupid_fkey", groupid, GroupTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
    /** Foreign key referencing UserTable (database name comment_userid_fkey) */
    lazy val userTableFk = foreignKey("comment_userid_fkey", userid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table CommentTable */
  lazy val CommentTable = new TableQuery(tag => new CommentTable(tag))

  /** Entity class storing rows of table GroupTable
   *
   * @param id        Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param groupname Database column groupname SqlType(varchar), Length(255,true)
   * @param groupdesc Database column groupdesc SqlType(text)
   * @param founderid Database column founderid SqlType(int4) */
  case class Group(id: Long, groupname: String, groupdesc: String, founderid: Long)

  object Group {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Group] = deriveDecoder
    implicit val jsonEncoder: Encoder[Group] = deriveEncoder
  }

  /** GetResult implicit for fetching Group objects using plain SQL queries */
  implicit def GetResultGroup(implicit e0: GR[Long], e1: GR[String]): GR[Group] = GR {
    prs =>
      import prs._
      Group.tupled((<<[Long], <<[String], <<[String], <<[Long]))
  }

  /** Table description of table group. Objects of this class serve as prototypes for rows in queries. */
  class GroupTable(_tableTag: Tag) extends profile.api.Table[Group](_tableTag, "group") {
    def * = (id, groupname, groupdesc, founderid) <> (Group.tupled, Group.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(groupname), Rep.Some(groupdesc), Rep.Some(founderid))).shaped.<>({ r => import r._; _1.map(_ => Group.tupled((_1.get, _2.get, _3.get, _4.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column groupname SqlType(varchar), Length(255,true) */
    val groupname: Rep[String] = column[String]("groupname", O.Length(255, varying = true))
    /** Database column groupdesc SqlType(text) */
    val groupdesc: Rep[String] = column[String]("groupdesc")
    /** Database column founderid SqlType(int4) */
    val founderid: Rep[Long] = column[Long]("founderid")

    /** Foreign key referencing UserTable (database name fk_user) */
    lazy val userTableFk = foreignKey("fk_user", founderid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)

    /** Uniqueness Index over (groupname) (database name group_groupname_key) */
    val index1 = index("group_groupname_key", groupname, unique = true)
  }

  /** Collection-like TableQuery object for table GroupTable */
  lazy val GroupTable = new TableQuery(tag => new GroupTable(tag))

  /** Entity class storing rows of table GrouptopicTable
   *
   * @param groupid Database column groupid SqlType(int4)
   * @param topicid Database column topicid SqlType(int4) */
  case class Grouptopic(groupid: Long, topicid: Long)

  object Grouptopic {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Grouptopic] = deriveDecoder
    implicit val jsonEncoder: Encoder[Grouptopic] = deriveEncoder
  }

  /** GetResult implicit for fetching Grouptopic objects using plain SQL queries */
  implicit def GetResultGrouptopic(implicit e0: GR[Long]): GR[Grouptopic] = GR {
    prs =>
      import prs._
      Grouptopic.tupled((<<[Long], <<[Long]))
  }

  /** Table description of table grouptopic. Objects of this class serve as prototypes for rows in queries. */
  class GrouptopicTable(_tableTag: Tag) extends profile.api.Table[Grouptopic](_tableTag, "grouptopic") {
    def * = (groupid, topicid) <> (Grouptopic.tupled, Grouptopic.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(groupid), Rep.Some(topicid))).shaped.<>({ r => import r._; _1.map(_ => Grouptopic.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column groupid SqlType(int4) */
    val groupid: Rep[Long] = column[Long]("groupid")
    /** Database column topicid SqlType(int4) */
    val topicid: Rep[Long] = column[Long]("topicid")

    /** Primary key of GrouptopicTable (database name grouptopic_pkey) */
    val pk = primaryKey("grouptopic_pkey", (groupid, topicid))

    /** Foreign key referencing GroupTable (database name grouptopic_groupid_fkey) */
    lazy val groupTableFk = foreignKey("grouptopic_groupid_fkey", groupid, GroupTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
    /** Foreign key referencing TopicTable (database name grouptopic_topicid_fkey) */
    lazy val topicTableFk = foreignKey("grouptopic_topicid_fkey", topicid, TopicTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table GrouptopicTable */
  lazy val GrouptopicTable = new TableQuery(tag => new GrouptopicTable(tag))

  /** Entity class storing rows of table MeetingTable
   *
   * @param id          Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param groupid     Database column groupid SqlType(int4)
   * @param description Database column description SqlType(text)
   * @param timeat      Database column timeat SqlType(timestamp) */
  case class Meeting(id: Long, groupid: Long, description: String, timeat: Option[java.time.Instant])

  object Meeting {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Meeting] = deriveDecoder
    implicit val jsonEncoder: Encoder[Meeting] = deriveEncoder
  }

  /** GetResult implicit for fetching Meeting objects using plain SQL queries */
  implicit def GetResultMeeting(implicit e0: GR[Long], e1: GR[String], e2: GR[Option[java.time.Instant]]): GR[Meeting] = GR {
    prs =>
      import prs._
      Meeting.tupled((<<[Long], <<[Long], <<[String], <<?[java.time.Instant]))
  }

  /** Table description of table meeting. Objects of this class serve as prototypes for rows in queries. */
  class MeetingTable(_tableTag: Tag) extends profile.api.Table[Meeting](_tableTag, "meeting") {
    def * = (id, groupid, description, timeat) <> (Meeting.tupled, Meeting.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(groupid), Rep.Some(description), timeat)).shaped.<>({ r => import r._; _1.map(_ => Meeting.tupled((_1.get, _2.get, _3.get, _4))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column groupid SqlType(int4) */
    val groupid: Rep[Long] = column[Long]("groupid")
    /** Database column description SqlType(text) */
    val description: Rep[String] = column[String]("description")
    /** Database column timeat SqlType(timestamp) */
    val timeat: Rep[Option[java.time.Instant]] = column[Option[java.time.Instant]]("timeat")

    /** Foreign key referencing GroupTable (database name meeting_groupid_fkey) */
    lazy val groupTableFk = foreignKey("meeting_groupid_fkey", groupid, GroupTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table MeetingTable */
  lazy val MeetingTable = new TableQuery(tag => new MeetingTable(tag))

  /** Entity class storing rows of table MessageTable
   *
   * @param id         Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param senderid   Database column senderid SqlType(int4)
   * @param receiverid Database column receiverid SqlType(int4)
   * @param text       Database column text SqlType(text)
   * @param timeat     Database column timeat SqlType(timestamp) */
  case class Message(id: Long, senderid: Long, receiverid: Long, text: String, timeat: Option[java.time.Instant])

  object Message {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Message] = deriveDecoder
    implicit val jsonEncoder: Encoder[Message] = deriveEncoder
  }

  /** GetResult implicit for fetching Message objects using plain SQL queries */
  implicit def GetResultMessage(implicit e0: GR[Long], e1: GR[String], e2: GR[Option[java.time.Instant]]): GR[Message] = GR {
    prs =>
      import prs._
      Message.tupled((<<[Long], <<[Long], <<[Long], <<[String], <<?[java.time.Instant]))
  }

  /** Table description of table message. Objects of this class serve as prototypes for rows in queries. */
  class MessageTable(_tableTag: Tag) extends profile.api.Table[Message](_tableTag, "message") {
    def * = (id, senderid, receiverid, text, timeat) <> (Message.tupled, Message.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(senderid), Rep.Some(receiverid), Rep.Some(text), timeat)).shaped.<>({ r => import r._; _1.map(_ => Message.tupled((_1.get, _2.get, _3.get, _4.get, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column senderid SqlType(int4) */
    val senderid: Rep[Long] = column[Long]("senderid")
    /** Database column receiverid SqlType(int4) */
    val receiverid: Rep[Long] = column[Long]("receiverid")
    /** Database column text SqlType(text) */
    val text: Rep[String] = column[String]("text")
    /** Database column timeat SqlType(timestamp) */
    val timeat: Rep[Option[java.time.Instant]] = column[Option[java.time.Instant]]("timeat")

    /** Foreign key referencing UserTable (database name message_receiverid_fkey) */
    lazy val userTableFk1 = foreignKey("message_receiverid_fkey", receiverid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
    /** Foreign key referencing UserTable (database name message_senderid_fkey) */
    lazy val userTableFk2 = foreignKey("message_senderid_fkey", senderid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table MessageTable */
  lazy val MessageTable = new TableQuery(tag => new MessageTable(tag))

  /** Entity class storing rows of table NotificationTable
   *
   * @param id      Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param groupid Database column groupid SqlType(int4)
   * @param userid  Database column userid SqlType(int4)
   * @param text    Database column text SqlType(text)
   * @param timeat  Database column timeat SqlType(timestamp) */
  case class Notification(id: Long, groupid: Long, userid: Long, text: String, timeat: Option[java.time.Instant])

  object Notification {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Notification] = deriveDecoder
    implicit val jsonEncoder: Encoder[Notification] = deriveEncoder
  }

  /** GetResult implicit for fetching Notification objects using plain SQL queries */
  implicit def GetResultNotification(implicit e0: GR[Long], e1: GR[String], e2: GR[Option[java.time.Instant]]): GR[Notification] = GR {
    prs =>
      import prs._
      Notification.tupled((<<[Long], <<[Long], <<[Long], <<[String], <<?[java.time.Instant]))
  }

  /** Table description of table notification. Objects of this class serve as prototypes for rows in queries. */
  class NotificationTable(_tableTag: Tag) extends profile.api.Table[Notification](_tableTag, "notification") {
    def * = (id, groupid, userid, text, timeat) <> (Notification.tupled, Notification.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(groupid), Rep.Some(userid), Rep.Some(text), timeat)).shaped.<>({ r => import r._; _1.map(_ => Notification.tupled((_1.get, _2.get, _3.get, _4.get, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column groupid SqlType(int4) */
    val groupid: Rep[Long] = column[Long]("groupid")
    /** Database column userid SqlType(int4) */
    val userid: Rep[Long] = column[Long]("userid")
    /** Database column text SqlType(text) */
    val text: Rep[String] = column[String]("text")
    /** Database column timeat SqlType(timestamp) */
    val timeat: Rep[Option[java.time.Instant]] = column[Option[java.time.Instant]]("timeat")

    /** Foreign key referencing GroupTable (database name notification_groupid_fkey) */
    lazy val groupTableFk = foreignKey("notification_groupid_fkey", groupid, GroupTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
    /** Foreign key referencing UserTable (database name notification_userid_fkey) */
    lazy val userTableFk = foreignKey("notification_userid_fkey", userid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table NotificationTable */
  lazy val NotificationTable = new TableQuery(tag => new NotificationTable(tag))

  /** Entity class storing rows of table TopicTable
   *
   * @param id   Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param name Database column name SqlType(varchar), Length(255,true) */
  case class Topic(id: Long, name: String)

  object Topic {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Topic] = deriveDecoder
    implicit val jsonEncoder: Encoder[Topic] = deriveEncoder
  }

  /** GetResult implicit for fetching Topic objects using plain SQL queries */
  implicit def GetResultTopic(implicit e0: GR[Long], e1: GR[String]): GR[Topic] = GR {
    prs =>
      import prs._
      Topic.tupled((<<[Long], <<[String]))
  }

  /** Table description of table topic. Objects of this class serve as prototypes for rows in queries. */
  class TopicTable(_tableTag: Tag) extends profile.api.Table[Topic](_tableTag, "topic") {
    def * = (id, name) <> (Topic.tupled, Topic.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(name))).shaped.<>({ r => import r._; _1.map(_ => Topic.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(varchar), Length(255,true) */
    val name: Rep[String] = column[String]("name", O.Length(255, varying = true))

    /** Uniqueness Index over (name) (database name topic_name_key) */
    val index1 = index("topic_name_key", name, unique = true)
  }

  /** Collection-like TableQuery object for table TopicTable */
  lazy val TopicTable = new TableQuery(tag => new TopicTable(tag))

  /** Entity class storing rows of table UsergroupTable
   *
   * @param userid  Database column userid SqlType(int4)
   * @param groupid Database column groupid SqlType(int4) */
  case class Usergroup(userid: Long, groupid: Long)

  object Usergroup {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Usergroup] = deriveDecoder
    implicit val jsonEncoder: Encoder[Usergroup] = deriveEncoder
  }

  /** GetResult implicit for fetching Usergroup objects using plain SQL queries */
  implicit def GetResultUsergroup(implicit e0: GR[Long]): GR[Usergroup] = GR {
    prs =>
      import prs._
      Usergroup.tupled((<<[Long], <<[Long]))
  }

  /** Table description of table usergroup. Objects of this class serve as prototypes for rows in queries. */
  class UsergroupTable(_tableTag: Tag) extends profile.api.Table[Usergroup](_tableTag, "usergroup") {
    def * = (userid, groupid) <> (Usergroup.tupled, Usergroup.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(userid), Rep.Some(groupid))).shaped.<>({ r => import r._; _1.map(_ => Usergroup.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column userid SqlType(int4) */
    val userid: Rep[Long] = column[Long]("userid")
    /** Database column groupid SqlType(int4) */
    val groupid: Rep[Long] = column[Long]("groupid")

    /** Primary key of UsergroupTable (database name usergroup_pkey) */
    val pk = primaryKey("usergroup_pkey", (userid, groupid))

    /** Foreign key referencing GroupTable (database name usergroup_groupid_fkey) */
    lazy val groupTableFk = foreignKey("usergroup_groupid_fkey", groupid, GroupTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
    /** Foreign key referencing UserTable (database name usergroup_userid_fkey) */
    lazy val userTableFk = foreignKey("usergroup_userid_fkey", userid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table UsergroupTable */
  lazy val UsergroupTable = new TableQuery(tag => new UsergroupTable(tag))

  /** Entity class storing rows of table UserTable
   *
   * @param id        Database column id SqlType(serial), AutoInc, PrimaryKey
   * @param firstname Database column firstname SqlType(varchar), Length(255,true), Default(None)
   * @param lastname  Database column lastname SqlType(varchar), Length(255,true), Default(None)
   * @param email     Database column email SqlType(varchar), Length(255,true)
   * @param hashed    Database column hashed SqlType(varchar), Length(255,true) */
  case class User(id: Long, firstname: Option[String] = None, lastname: Option[String] = None, email: String, hashed: String)

  object User {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[User] = deriveDecoder
    implicit val jsonEncoder: Encoder[User] = deriveEncoder
  }

  /** GetResult implicit for fetching User objects using plain SQL queries */
  implicit def GetResultUser(implicit e0: GR[Long], e1: GR[Option[String]], e2: GR[String]): GR[User] = GR {
    prs =>
      import prs._
      User.tupled((<<[Long], <<?[String], <<?[String], <<[String], <<[String]))
  }

  /** Table description of table user. Objects of this class serve as prototypes for rows in queries. */
  class UserTable(_tableTag: Tag) extends profile.api.Table[User](_tableTag, "user") {
    def * = (id, firstname, lastname, email, hashed) <> (User.tupled, User.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), firstname, lastname, Rep.Some(email), Rep.Some(hashed))).shaped.<>({ r => import r._; _1.map(_ => User.tupled((_1.get, _2, _3, _4.get, _5.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column firstname SqlType(varchar), Length(255,true), Default(None) */
    val firstname: Rep[Option[String]] = column[Option[String]]("firstname", O.Length(255, varying = true), O.Default(None))
    /** Database column lastname SqlType(varchar), Length(255,true), Default(None) */
    val lastname: Rep[Option[String]] = column[Option[String]]("lastname", O.Length(255, varying = true), O.Default(None))
    /** Database column email SqlType(varchar), Length(255,true) */
    val email: Rep[String] = column[String]("email", O.Length(255, varying = true))
    /** Database column hashed SqlType(varchar), Length(255,true) */
    val hashed: Rep[String] = column[String]("hashed", O.Length(255, varying = true))

    /** Uniqueness Index over (email) (database name user_email_key) */
    val index1 = index("user_email_key", email, unique = true)
    /** Uniqueness Index over (hashed) (database name user_hashed_key) */
    val index2 = index("user_hashed_key", hashed, unique = true)
  }

  /** Collection-like TableQuery object for table UserTable */
  lazy val UserTable = new TableQuery(tag => new UserTable(tag))

  /** Entity class storing rows of table UsertopicTable
   *
   * @param userid  Database column userid SqlType(int4)
   * @param topicid Database column topicid SqlType(int4) */
  case class Usertopic(userid: Long, topicid: Long)

  object Usertopic {
    // This definition is required for proper
    // slick functionality with companion objects
    val tupled = (this.apply _).tupled
    implicit val jsonDecoder: Decoder[Usertopic] = deriveDecoder
    implicit val jsonEncoder: Encoder[Usertopic] = deriveEncoder
  }

  /** GetResult implicit for fetching Usertopic objects using plain SQL queries */
  implicit def GetResultUsertopic(implicit e0: GR[Long]): GR[Usertopic] = GR {
    prs =>
      import prs._
      Usertopic.tupled((<<[Long], <<[Long]))
  }

  /** Table description of table usertopic. Objects of this class serve as prototypes for rows in queries. */
  class UsertopicTable(_tableTag: Tag) extends profile.api.Table[Usertopic](_tableTag, "usertopic") {
    def * = (userid, topicid) <> (Usertopic.tupled, Usertopic.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(userid), Rep.Some(topicid))).shaped.<>({ r => import r._; _1.map(_ => Usertopic.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column userid SqlType(int4) */
    val userid: Rep[Long] = column[Long]("userid")
    /** Database column topicid SqlType(int4) */
    val topicid: Rep[Long] = column[Long]("topicid")

    /** Primary key of UsertopicTable (database name usertopic_pkey) */
    val pk = primaryKey("usertopic_pkey", (userid, topicid))

    /** Foreign key referencing TopicTable (database name usertopic_topicid_fkey) */
    lazy val topicTableFk = foreignKey("usertopic_topicid_fkey", topicid, TopicTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
    /** Foreign key referencing UserTable (database name usertopic_userid_fkey) */
    lazy val userTableFk = foreignKey("usertopic_userid_fkey", userid, UserTable)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.Cascade)
  }

  /** Collection-like TableQuery object for table UsertopicTable */
  lazy val UsertopicTable = new TableQuery(tag => new UsertopicTable(tag))
}
